
#include "vm_obstacle_detector/vm_obstacle_detector.h"




VMInObstacle::VMInObstacle(tf::TransformListener* tf_transform_listener)
{
    tf_transform_listener_=tf_transform_listener;

    flag_pose_vm_wrt_world_=false;

    return;
}

void VMInObstacle::setVMRefFrame(std::string vm_ref_frame)
{
    vm_ref_frame_=vm_ref_frame;
}

std::string VMInObstacle::getVMRefFrame() const
{
    return vm_ref_frame_;
}

void VMInObstacle::setWorldRefFrame(std::string world_ref_frame)
{
    world_ref_frame_=world_ref_frame;
}

bool VMInObstacle::isMapped()
{
    return flag_pose_vm_wrt_world_;
}

int VMInObstacle::listenTransform()
{

    try
    {
        tf_transform_listener_->lookupTransform(world_ref_frame_, vm_ref_frame_,
                           ros::Time(0), transform_pose_vm_wrt_world_);

        flag_pose_vm_wrt_world_=true;
    }
    catch(tf::TransformException &ex)
    {
        flag_pose_vm_wrt_world_=false;
        return 1;
    }


    // Position
    tf::Vector3 position_vm_wrt_world=transform_pose_vm_wrt_world_.getOrigin();

    position_vm_wrt_world_(0)=position_vm_wrt_world.getX();
    position_vm_wrt_world_(1)=position_vm_wrt_world.getY();
    position_vm_wrt_world_(2)=position_vm_wrt_world.getZ();


    // Attitude
    tf::Quaternion attitude_vm_wrt_world=transform_pose_vm_wrt_world_.getRotation();

    attitude_vm_wrt_world_(0)=attitude_vm_wrt_world.getW();
    attitude_vm_wrt_world_(1)=attitude_vm_wrt_world.getX();
    attitude_vm_wrt_world_(2)=attitude_vm_wrt_world.getY();
    attitude_vm_wrt_world_(3)=attitude_vm_wrt_world.getZ();


    return 0;
}

int VMInObstacle::configureVMInObstacle(const pugi::xml_node& vm_in_obstacle_node)
{
    // Aux vars
    std::string reading_value;

    // Id
    reading_value=vm_in_obstacle_node.child_value("id");
    int id;
    if(!reading_value.empty())
    {
        id=std::stoi(reading_value);
        //
        setVMRefFrame("visual_marker_"+std::to_string(id));
    }

    // Position in obstacle
    reading_value=vm_in_obstacle_node.child_value("position_wrt_obstacle");
    if(!reading_value.empty())
    {
        std::istringstream stm(reading_value);
        stm>>position_vm_wrt_obstacle_[0]>>position_vm_wrt_obstacle_[1]>>position_vm_wrt_obstacle_[2];
    }

    // Attitude in obstacle
    reading_value=vm_in_obstacle_node.child_value("attitude_wrt_obstacle");
    if(!reading_value.empty())
    {
        std::istringstream stm(reading_value);
        stm>>attitude_vm_wrt_obstacle_[0]>>attitude_vm_wrt_obstacle_[1]>>attitude_vm_wrt_obstacle_[2]>>attitude_vm_wrt_obstacle_[3];
    }
    attitude_vm_wrt_obstacle_/=attitude_vm_wrt_obstacle_.norm();


    ROS_INFO("-VM added with id=%d",id);

    // end
    return 0;
}

int VMInObstacle::getPoseObstacleWrtWorld(Eigen::Vector3d& position_obstacle_wrt_world, Eigen::Vector4d& attitude_obstacle_wrt_world)
{
    // check
    if(!isMapped())
        return 1;

    // Position
    Eigen::Vector3d position_obstacle_wrt_vm=-Quaternion::cross_sandwich(
                Quaternion::conj(attitude_vm_wrt_obstacle_),
                position_vm_wrt_obstacle_,
                attitude_vm_wrt_obstacle_);

    position_obstacle_wrt_world=
            Quaternion::cross_sandwich(
                attitude_vm_wrt_world_,
                position_obstacle_wrt_vm,
                Quaternion::conj(attitude_vm_wrt_world_))
            +
            position_vm_wrt_world_;

    // Attitude
    attitude_obstacle_wrt_world=Quaternion::cross(attitude_vm_wrt_world_, Quaternion::conj(attitude_vm_wrt_obstacle_));


    // end
    return 0;
}







VMObstacle::VMObstacle(tf::TransformListener* tf_transform_listener)
{
    tf_transform_listener_=tf_transform_listener;

    return;
}

void VMObstacle::setWorldRefFrame(std::string world_ref_frame)
{
    world_ref_frame_=world_ref_frame;
}

void VMObstacle::setId(int id)
{
    id_=id;
}

int VMObstacle::getId() const
{
    return id_;
}

int VMObstacle::configureVMObstacle(const pugi::xml_node& obstacle_node)
{
    // Aux vars
    std::string reading_value;

    // Size
    reading_value=obstacle_node.child_value("size");
    if(!reading_value.empty())
    {
        std::istringstream stm(reading_value);
        Eigen::Vector3d size_obstacle_wrt_world;
        stm>>size_obstacle_wrt_world[0]>>size_obstacle_wrt_world[1]>>size_obstacle_wrt_world[2];
        setSizeObstacleWrtWorld(size_obstacle_wrt_world);
    }


    pugi::xml_node vm_in_obstacle=obstacle_node.child("vm_in_obstacle");
    for(pugi::xml_node vm_node = vm_in_obstacle.child("vm"); vm_node; vm_node = vm_node.next_sibling("vm"))
    {
        // Create VM in obstacle
        VMInObstacle vm(tf_transform_listener_);

        // Configure
        vm.setWorldRefFrame(world_ref_frame_);

        vm.configureVMInObstacle(vm_node);

        // push back
        list_vm_in_obstacle_.push_back(vm);

    }

    // end
    return 0;
}

int VMObstacle::listenTransformVMInObstacle()
{
    // iterate
    for(std::list<VMInObstacle>::iterator it_list_vm_in_obstacle=list_vm_in_obstacle_.begin();
        it_list_vm_in_obstacle!=list_vm_in_obstacle_.end();
        ++it_list_vm_in_obstacle)
    {
        it_list_vm_in_obstacle->listenTransform();
    }

    return 0;
}

int VMObstacle::getNumberMappedVisualMarkers()
{
    int number_mapped_visual_markers=0;
    for(std::list<VMInObstacle>::iterator it_list_vm_in_obstacle=list_vm_in_obstacle_.begin();
        it_list_vm_in_obstacle!=list_vm_in_obstacle_.end();
        ++it_list_vm_in_obstacle)
    {
        if(it_list_vm_in_obstacle->isMapped())
            number_mapped_visual_markers++;
    }
    return number_mapped_visual_markers;
}

void VMObstacle::setSizeObstacleWrtWorld(const Eigen::Vector3d& size_obstacle_wrt_world)
{
    size_obstacle_wrt_world_=size_obstacle_wrt_world;
}

int VMObstacle::fillMessage(visualization_msgs::Marker& pose_obstacle_wrt_world)
{
    fillMessageCommon(pose_obstacle_wrt_world);

    fillMessageSpecific(pose_obstacle_wrt_world);

    return 0;
}

int VMObstacle::fillMessageCommon(visualization_msgs::Marker& pose_obstacle_wrt_world)
{
    // Header
    pose_obstacle_wrt_world.header.stamp=ros::Time::now();
    pose_obstacle_wrt_world.header.frame_id=world_ref_frame_;

    // ns
    pose_obstacle_wrt_world.ns="vm_obstacles";

    // id
    pose_obstacle_wrt_world.id=id_;

    // action
    pose_obstacle_wrt_world.action=visualization_msgs::Marker::ADD;

    // pose
    pose_obstacle_wrt_world.pose.position.x=position_obstacle_wrt_world_(0);
    pose_obstacle_wrt_world.pose.position.y=position_obstacle_wrt_world_(1);
    pose_obstacle_wrt_world.pose.position.z=position_obstacle_wrt_world_(2);
    pose_obstacle_wrt_world.pose.orientation.w=attitude_obstacle_wrt_world_(0);
    pose_obstacle_wrt_world.pose.orientation.x=attitude_obstacle_wrt_world_(1);
    pose_obstacle_wrt_world.pose.orientation.y=attitude_obstacle_wrt_world_(2);
    pose_obstacle_wrt_world.pose.orientation.z=attitude_obstacle_wrt_world_(3);

    // scale
    pose_obstacle_wrt_world.scale.x=size_obstacle_wrt_world_(0);
    pose_obstacle_wrt_world.scale.y=size_obstacle_wrt_world_(1);
    pose_obstacle_wrt_world.scale.z=size_obstacle_wrt_world_(2);


    // lifetime
    pose_obstacle_wrt_world.lifetime=ros::Duration(0);

    // frame_locked
    pose_obstacle_wrt_world.frame_locked=true;

    // Other fields not needed

    // end
    return 0;
}

int VMObstacle::obtainPoseObstacleWrtWorld(visualization_msgs::Marker& pose_obstacle_wrt_world)
{
    // listen transform VM in obstacle
    listenTransformVMInObstacle();

    // calculate obstacle wrt world
    int error_calculate_obstacle_parameters=calculateObstacleParameters();
    if(error_calculate_obstacle_parameters)
        return 1;

    // create the ros msgs
    fillMessage(pose_obstacle_wrt_world);

    // end
    return 0;
}







VMObstacleCube::VMObstacleCube(tf::TransformListener* tf_transform_listener) :
    VMObstacle(tf_transform_listener)
{

    return;
}

int VMObstacleCube::calculateObstacleParameters()
{
    // Center

    //bool flag_pose_obstacle_calculated=false;

    // Average
    int number_mapped_visual_markers=getNumberMappedVisualMarkers();

    // Check
    if(number_mapped_visual_markers==0)
        return 1;

    // Weigth
    double weigth_per_mapped_visual_marker=1/(static_cast<double>(number_mapped_visual_markers));
    int mapped_visual_marker_i=0;

    // Average positions
    // Average quaternions:http://stackoverflow.com/questions/12374087/average-of-multiple-quaternions
    Eigen::MatrixXd average_quaternion_matrix(4, number_mapped_visual_markers);

    // Set zero
    position_obstacle_wrt_world_.setZero();
    attitude_obstacle_wrt_world_.setZero();

    // Loop
    for(std::list<VMInObstacle>::iterator it_list_vm_in_obstacle=list_vm_in_obstacle_.begin();
        it_list_vm_in_obstacle!=list_vm_in_obstacle_.end();
        ++it_list_vm_in_obstacle)
    {
        Eigen::Vector3d position_obstacle_wrt_world;
        Eigen::Vector4d attitude_obstacle_wrt_world;

        int error=it_list_vm_in_obstacle->getPoseObstacleWrtWorld(position_obstacle_wrt_world, attitude_obstacle_wrt_world);
        if(!error)
        {
            position_obstacle_wrt_world_+=weigth_per_mapped_visual_marker*position_obstacle_wrt_world;

            average_quaternion_matrix.block<4,1>(0,mapped_visual_marker_i)=weigth_per_mapped_visual_marker*attitude_obstacle_wrt_world;

            //flag_pose_obstacle_calculated=true;
            //break;

            mapped_visual_marker_i++;
        }
    }

    // Check
//    if(!flag_pose_obstacle_calculated)
//        return 1;

    Eigen::Matrix4d total_average_quaternion_matrix=average_quaternion_matrix*average_quaternion_matrix.transpose();

    Eigen::SelfAdjointEigenSolver<Eigen::Matrix4d> eigensolver(total_average_quaternion_matrix);
    if (eigensolver.info() != Eigen::Success)
        return -1;

    Eigen::Vector4d eigenvalues=eigensolver.eigenvalues();
    Eigen::Matrix4d eigenvector=eigensolver.eigenvectors();

    double max=0;
    for(int i=0; i<4; i++)
    {
        if(eigenvalues(i)>max)
        {
            max=eigenvalues(i);
            attitude_obstacle_wrt_world_=eigenvector.block<4,1>(0,i);
        }
    }

    // Attitude average
    attitude_obstacle_wrt_world_/=attitude_obstacle_wrt_world_.norm();


    // Size
    // Do not estimate, use default value

    // end
    return 0;
}

int VMObstacleCube::fillMessageSpecific(visualization_msgs::Marker& pose_obstacle_wrt_world)
{

    // color
    pose_obstacle_wrt_world.color.r=0.8;
    pose_obstacle_wrt_world.color.g=0.8;
    pose_obstacle_wrt_world.color.b=0.8;
    pose_obstacle_wrt_world.color.a=0.9;


    // type
    pose_obstacle_wrt_world.type=visualization_msgs::Marker::CUBE;


    return 0;
}











VMObstacleSphere::VMObstacleSphere(tf::TransformListener* tf_transform_listener) :
    VMObstacle(tf_transform_listener)
{

    return;
}

int VMObstacleSphere::calculateObstacleParameters()
{
    // Center

    //bool flag_pose_obstacle_calculated=false;

    // Average
    int number_mapped_visual_markers=getNumberMappedVisualMarkers();

    // Check
    if(number_mapped_visual_markers==0)
        return 1;

    // Weigth
    double weigth_per_mapped_visual_marker=1/(static_cast<double>(number_mapped_visual_markers));
    int mapped_visual_marker_i=0;

    // Average positions
    // Average quaternions:http://stackoverflow.com/questions/12374087/average-of-multiple-quaternions
    Eigen::MatrixXd average_quaternion_matrix(4, number_mapped_visual_markers);

    // Set zero
    position_obstacle_wrt_world_.setZero();
    attitude_obstacle_wrt_world_.setZero();

    // Loop
    for(std::list<VMInObstacle>::iterator it_list_vm_in_obstacle=list_vm_in_obstacle_.begin();
        it_list_vm_in_obstacle!=list_vm_in_obstacle_.end();
        ++it_list_vm_in_obstacle)
    {
        Eigen::Vector3d position_obstacle_wrt_world;
        Eigen::Vector4d attitude_obstacle_wrt_world;

        int error=it_list_vm_in_obstacle->getPoseObstacleWrtWorld(position_obstacle_wrt_world, attitude_obstacle_wrt_world);
        if(!error)
        {
            position_obstacle_wrt_world_+=weigth_per_mapped_visual_marker*position_obstacle_wrt_world;

            average_quaternion_matrix.block<4,1>(0,mapped_visual_marker_i)=weigth_per_mapped_visual_marker*attitude_obstacle_wrt_world;

            //flag_pose_obstacle_calculated=true;
            //break;

            mapped_visual_marker_i++;
        }
    }

    // Check
//    if(!flag_pose_obstacle_calculated)
//        return 1;

    Eigen::Matrix4d total_average_quaternion_matrix=average_quaternion_matrix*average_quaternion_matrix.transpose();

    Eigen::SelfAdjointEigenSolver<Eigen::Matrix4d> eigensolver(total_average_quaternion_matrix);
    if (eigensolver.info() != Eigen::Success)
        return -1;

    Eigen::Vector4d eigenvalues=eigensolver.eigenvalues();
    Eigen::Matrix4d eigenvector=eigensolver.eigenvectors();

    double max=0;
    for(int i=0; i<4; i++)
    {
        if(eigenvalues(i)>max)
        {
            max=eigenvalues(i);
            attitude_obstacle_wrt_world_=eigenvector.block<4,1>(0,i);
        }
    }

    // Attitude average
    attitude_obstacle_wrt_world_/=attitude_obstacle_wrt_world_.norm();


    // Size
    // Do not estimate, use default value

    // end
    return 0;
}

int VMObstacleSphere::fillMessageSpecific(visualization_msgs::Marker& pose_obstacle_wrt_world)
{
    // color
    pose_obstacle_wrt_world.color.r=0.6;
    pose_obstacle_wrt_world.color.g=0.6;
    pose_obstacle_wrt_world.color.b=0.6;
    pose_obstacle_wrt_world.color.a=0.9;

    // type
    pose_obstacle_wrt_world.type=visualization_msgs::Marker::SPHERE;


    return 0;
}












VMObstacleCylinder::VMObstacleCylinder(tf::TransformListener* tf_transform_listener) :
    VMObstacle(tf_transform_listener)
{

    return;
}

int VMObstacleCylinder::calculateObstacleParameters()
{
    // Center

    //bool flag_pose_obstacle_calculated=false;

    // Average
    int number_mapped_visual_markers=getNumberMappedVisualMarkers();

    // Check
    if(number_mapped_visual_markers==0)
        return 1;

    // Weigth
    double weigth_per_mapped_visual_marker=1/(static_cast<double>(number_mapped_visual_markers));
    int mapped_visual_marker_i=0;

    // Average positions
    // Average quaternions:http://stackoverflow.com/questions/12374087/average-of-multiple-quaternions
    Eigen::MatrixXd average_quaternion_matrix(4, number_mapped_visual_markers);

    // Set zero
    position_obstacle_wrt_world_.setZero();
    attitude_obstacle_wrt_world_.setZero();

    // Loop
    for(std::list<VMInObstacle>::iterator it_list_vm_in_obstacle=list_vm_in_obstacle_.begin();
        it_list_vm_in_obstacle!=list_vm_in_obstacle_.end();
        ++it_list_vm_in_obstacle)
    {
        Eigen::Vector3d position_obstacle_wrt_world;
        Eigen::Vector4d attitude_obstacle_wrt_world;

        int error=it_list_vm_in_obstacle->getPoseObstacleWrtWorld(position_obstacle_wrt_world, attitude_obstacle_wrt_world);
        if(!error)
        {
            position_obstacle_wrt_world_+=weigth_per_mapped_visual_marker*position_obstacle_wrt_world;

            average_quaternion_matrix.block<4,1>(0,mapped_visual_marker_i)=weigth_per_mapped_visual_marker*attitude_obstacle_wrt_world;

            //flag_pose_obstacle_calculated=true;
            //break;

            mapped_visual_marker_i++;
        }
    }

    // Check
//    if(!flag_pose_obstacle_calculated)
//        return 1;

    Eigen::Matrix4d total_average_quaternion_matrix=average_quaternion_matrix*average_quaternion_matrix.transpose();

    Eigen::SelfAdjointEigenSolver<Eigen::Matrix4d> eigensolver(total_average_quaternion_matrix);
    if (eigensolver.info() != Eigen::Success)
        return -1;

    Eigen::Vector4d eigenvalues=eigensolver.eigenvalues();
    Eigen::Matrix4d eigenvector=eigensolver.eigenvectors();

    double max=0;
    for(int i=0; i<4; i++)
    {
        if(eigenvalues(i)>max)
        {
            max=eigenvalues(i);
            attitude_obstacle_wrt_world_=eigenvector.block<4,1>(0,i);
        }
    }

    // Attitude average
    attitude_obstacle_wrt_world_/=attitude_obstacle_wrt_world_.norm();


    // Size
    // Do not estimate, use default value

    // end
    return 0;
}

int VMObstacleCylinder::fillMessageSpecific(visualization_msgs::Marker& pose_obstacle_wrt_world)
{
    // color
    pose_obstacle_wrt_world.color.r=0.4;
    pose_obstacle_wrt_world.color.g=0.4;
    pose_obstacle_wrt_world.color.b=0.4;
    pose_obstacle_wrt_world.color.a=0.9;

    // type
    pose_obstacle_wrt_world.type=visualization_msgs::Marker::CYLINDER;


    return 0;
}












VMObstacleDetector::VMObstacleDetector(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // Init
    init();

    // Create
    create();

    // Read parameters
    readParameters();

    // End
    return;
}

VMObstacleDetector::~VMObstacleDetector()
{
    // clear list
    obstacles_list_.clear();

    return;
}

int VMObstacleDetector::init()
{
    // Defautl values

    rate_val_=10.0;

    world_ref_frame_="world";

    config_file_path_="config_file.xml";

    obstacles_topic_name_="obstacles_static";

    last_used_id_=0;


    // End
    return 0;
}

int VMObstacleDetector::create()
{
    // tf listener
    tf_transform_listener_=new tf::TransformListener;
    // tf broadcaster
    //tf_transform_broadcaster_=new tf::TransformBroadcaster;

    //
    rate_=new ros::Rate(rate_val_);

    // End
    return 0;
}

int VMObstacleDetector::readParameters()
{
    // Config files
    //
    ros::param::param<std::string>("~vm_obstacle_detector_config_file", config_file_path_, "vm_obstacle_detector_config_file.xml");
    ROS_INFO("vm_obstacle_detector_config_file=%s",config_file_path_.c_str());

    // end
    return 0;
}

int VMObstacleDetector::readConfigFile()
{


    // File
    if ( !boost::filesystem::exists( config_file_path_ ) )
    {
        std::cout << "Can't find the file: "<< config_file_path_ << std::endl;
        return 1;
    }

    //XML document
    pugi::xml_document doc;
    std::ifstream name_file(config_file_path_.c_str());
    pugi::xml_parse_result result = doc.load(name_file);

    if(!result)
    {
        std::cout<<"I cannot open xml file: "<<config_file_path_<<std::endl;
        return 2;
    }

    //
    pugi::xml_node vm_obstacle_detector_node = doc.child("vm_obstacle_detector");


    // Reading cubes
    for(pugi::xml_node cube_node = vm_obstacle_detector_node.child("cube"); cube_node; cube_node = cube_node.next_sibling("cube"))
    {
        // Create
        VMObstacleCube* cube=new VMObstacleCube(tf_transform_listener_);

        // Configure
        // World
        cube->setWorldRefFrame(world_ref_frame_);

        // id
        std::string id_str=cube_node.child_value("id");
        int id_int=-1;
        if(!id_str.empty())
        {
            std::istringstream stm(id_str);
            stm>>id_int;
            if(findObstacleWithId(id_int))
            {
                ROS_ERROR("ID already used. Setting other id");
                id_int=-1;
            }
        }
        if(id_int == -1)
        {
            // search if it is already used
            while(findObstacleWithId(last_used_id_))
            {
                last_used_id_++;
            }
            id_int=last_used_id_;
        }
        cube->setId(id_int);

        // INFO
        ROS_INFO("Cube created with id=%d",cube->getId());

        // VM
        cube->configureVMObstacle(cube_node);


        // Push to the list
        obstacles_list_.push_back(cube);
    }

    // Reading spheres
    for(pugi::xml_node sphere_node = vm_obstacle_detector_node.child("sphere"); sphere_node; sphere_node = sphere_node.next_sibling("sphere"))
    {
        // Create
        VMObstacleSphere* sphere=new VMObstacleSphere(tf_transform_listener_);

        // Configure
        // World
        sphere->setWorldRefFrame(world_ref_frame_);

        // id
        std::string id_str=sphere_node.child_value("id");
        int id_int=-1;
        if(!id_str.empty())
        {
            std::istringstream stm(id_str);
            stm>>id_int;
            if(findObstacleWithId(id_int))
            {
                ROS_ERROR("ID already used. Setting other id");
                id_int=-1;
            }
        }
        if(id_int == -1)
        {
            // search if it is already used
            while(findObstacleWithId(last_used_id_))
            {
                last_used_id_++;
            }
            id_int=last_used_id_;
        }
        sphere->setId(id_int);

        // INFO
        ROS_INFO("Sphere created with id=%d",sphere->getId());

        // VM
        sphere->configureVMObstacle(sphere_node);


        // Push to the list
        obstacles_list_.push_back(sphere);
    }

    // Reading cylinders
    for(pugi::xml_node cylinder_node = vm_obstacle_detector_node.child("cylinder"); cylinder_node; cylinder_node = cylinder_node.next_sibling("cylinder"))
    {
        // Create
        VMObstacleCylinder* cylinder=new VMObstacleCylinder(tf_transform_listener_);

        // Configure
        // World
        cylinder->setWorldRefFrame(world_ref_frame_);

        // id
        std::string id_str=cylinder_node.child_value("id");
        int id_int=-1;
        if(!id_str.empty())
        {
            std::istringstream stm(id_str);
            stm>>id_int;
            if(findObstacleWithId(id_int))
            {
                ROS_ERROR("ID already used. Setting other id");
                id_int=-1;
            }
        }
        if(id_int == -1)
        {
            // search if it is already used
            while(findObstacleWithId(last_used_id_))
            {
                last_used_id_++;
            }
            id_int=last_used_id_;
        }
        cylinder->setId(id_int);

        // INFO
        ROS_INFO("Cylinder created with id=%d",cylinder->getId());

        // VM
        cylinder->configureVMObstacle(cylinder_node);


        // Push to the list
        obstacles_list_.push_back(cylinder);
    }


    // Other kind of obstacles
    // TODO


    // End
    return 0;
}

int VMObstacleDetector::open()
{
    // Read config file
    readConfigFile();

    // Open topics

    // Publishers
    obstacles_pub_ = nh_->advertise<visualization_msgs::MarkerArray>(obstacles_topic_name_, 10);

    // End
    return 0;
}

int VMObstacleDetector::close()
{

    return 0;
}

int VMObstacleDetector::run()
{
    // Loop
    try
    {
        while(ros::ok())
        {
            // Spin
            ros::spinOnce();

            // Clear msg
            obstacles_msg_.markers.clear();

            // Iterate
            for(std::list<VMObstacle*>::iterator it_obstacles_list=obstacles_list_.begin();
                it_obstacles_list!=obstacles_list_.end();
                ++it_obstacles_list)
            {
                visualization_msgs::Marker pose_obstacle_wrt_world;

                // Get pose
                int error_obtain_pose_obstacle_wrt_world=(*it_obstacles_list)->obtainPoseObstacleWrtWorld(pose_obstacle_wrt_world);
                if(error_obtain_pose_obstacle_wrt_world)
                    continue;

                // Add to the marker list
                obstacles_msg_.markers.push_back(pose_obstacle_wrt_world);
            }

            // Publish
            publishObstacles();


            // Sleep
            rate_->sleep();
        }
    }
    catch(std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception: "<<ex.what()<<std::endl;
    }
    catch(...)
    {
        std::cout<<"EXCEPTION ON main thread"<<std::endl;
    }

    return 0;
}

int VMObstacleDetector::publishObstacles()
{
    if(!obstacles_msg_.markers.empty())
        obstacles_pub_.publish(obstacles_msg_);

    return 0;
}

bool VMObstacleDetector::findObstacleWithId(int id) const
{
    for(std::list<VMObstacle*>::const_iterator it_obstacles_list=obstacles_list_.begin();
        it_obstacles_list!=obstacles_list_.end();
        ++it_obstacles_list)
    {
        if((*it_obstacles_list)->getId() == id)
            return true;
    }
    return false;
}

//////////////////////////////////////////////////////
//  .cpp
//
//  Created on: Sep, 2016
//      Author: joselusl
//
//  Last modification on:
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O stream
//std::cout
#include <iostream>


//
#include "vm_obstacle_detector/vm_obstacle_detector.h"



int main(int argc,char **argv)
{
    VMObstacleDetector vm_obstacle_detector(argc, argv);
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());

    vm_obstacle_detector.open();

    vm_obstacle_detector.run();

    return 0;
}

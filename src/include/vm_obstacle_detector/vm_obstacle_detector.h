
// string
#include <string>

// list
#include <list>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


// Eigen
#include <Eigen/Eigen>

// Boost
#include <boost/filesystem.hpp>


// ros
#include <ros/ros.h>

// tf
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
//#include <tf/transform_broadcaster.h>

// visualization msgs
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

// pugixml
#include "pugixml/pugixml.hpp"


// quaternion algebra
#include "quaternion_algebra/quaternion_algebra.h"




////////////////////////////////
/// \brief The VMInObstacle class
/////////////////////////////////
class VMInObstacle
{
public:
    VMInObstacle(tf::TransformListener* tf_transform_listener);


    // Tf
protected:
    tf::TransformListener* tf_transform_listener_;


protected:
    std::string vm_ref_frame_;
public:
    void setVMRefFrame(std::string vm_ref_frame);
    std::string getVMRefFrame() const;

protected:
    std::string world_ref_frame_;
public:
    void setWorldRefFrame(std::string world_ref_frame);

    // Pose vm wrt World
protected:
    bool flag_pose_vm_wrt_world_;
public:
    bool isMapped();
protected:
    tf::StampedTransform transform_pose_vm_wrt_world_;
    Eigen::Vector3d position_vm_wrt_world_;
    Eigen::Vector4d attitude_vm_wrt_world_;

    // Pose vm wrt Obstacle
protected:
    Eigen::Vector3d position_vm_wrt_obstacle_;
    Eigen::Vector4d attitude_vm_wrt_obstacle_;



public:
    int listenTransform();



public:
    int configureVMInObstacle(const pugi::xml_node& vm_in_obstacle_node);


public:
    int getPoseObstacleWrtWorld(Eigen::Vector3d& position_obstacle_wrt_world, Eigen::Vector4d& attitude_obstacle_wrt_world);


};






////////////////////////////////////
/// \brief The VMObstacle class
///////////////////////////////////
class VMObstacle
{
public:
    VMObstacle(tf::TransformListener* tf_transform_listener);

    // Tf
protected:
    tf::TransformListener* tf_transform_listener_;


protected:
    std::string world_ref_frame_;
public:
    void setWorldRefFrame(std::string world_ref_frame);

protected:
    std::list<VMInObstacle> list_vm_in_obstacle_;

    // id
protected:
    int id_;
public:
    void setId(int id);
    int getId() const;

    // Color
protected:


public:
    int configureVMObstacle(const pugi::xml_node& obstacle_node);

protected:
    int listenTransformVMInObstacle();


    // number mapped visual markers
protected:
    int getNumberMappedVisualMarkers();


    // parameters obstacle
protected:
    Eigen::Vector3d position_obstacle_wrt_world_;
    Eigen::Vector4d attitude_obstacle_wrt_world_;
    Eigen::Vector3d size_obstacle_wrt_world_;

public:
    void setSizeObstacleWrtWorld(const Eigen::Vector3d& size_obstacle_wrt_world);

protected:
    virtual int calculateObstacleParameters()=0;


protected:
    int fillMessage(visualization_msgs::Marker& pose_obstacle_wrt_world);
    int fillMessageCommon(visualization_msgs::Marker& pose_obstacle_wrt_world);
    virtual int fillMessageSpecific(visualization_msgs::Marker& pose_obstacle_wrt_world)=0;



public:
    int obtainPoseObstacleWrtWorld(visualization_msgs::Marker& pose_obstacle_wrt_world);


};




//////////////////////////////////////
/// \brief The VMObstacleCube class
//////////////////////////////////////
class VMObstacleCube : public VMObstacle
{
public:
    VMObstacleCube(tf::TransformListener* tf_transform_listener);


protected:
    int calculateObstacleParameters();

protected:
    int fillMessageSpecific(visualization_msgs::Marker& pose_obstacle_wrt_world);


};





//////////////////////////////////////
/// \brief The VMObstacleSphere class
//////////////////////////////////////
class VMObstacleSphere : public VMObstacle
{
public:
    VMObstacleSphere(tf::TransformListener* tf_transform_listener);


protected:
    int calculateObstacleParameters();

protected:
    int fillMessageSpecific(visualization_msgs::Marker& pose_obstacle_wrt_world);


};





//////////////////////////////////////
/// \brief The VMObstacleCylinder class
//////////////////////////////////////
class VMObstacleCylinder : public VMObstacle
{
public:
    VMObstacleCylinder(tf::TransformListener* tf_transform_listener);


protected:
    int calculateObstacleParameters();

protected:
    int fillMessageSpecific(visualization_msgs::Marker& pose_obstacle_wrt_world);


};




//////////////////////////////////
/// \brief The VMObstacleDetector class
/////////////////////////////////
class VMObstacleDetector
{
public:
    VMObstacleDetector(int argc,char **argv);
    ~VMObstacleDetector();



protected:
    ros::NodeHandle* nh_;


    // Tf
protected:
    tf::TransformListener* tf_transform_listener_;
    // tf::TransformBroadcaster* tf_transform_broadcaster_;

protected:
    std::list<VMObstacle*> obstacles_list_;

public:
    bool findObstacleWithId(int id) const;


    // Obstacles publisher
protected:
    std::string obstacles_topic_name_;
    ros::Publisher obstacles_pub_;
    visualization_msgs::MarkerArray obstacles_msg_;
    int publishObstacles();


    // World ref frame
protected:
    std::string world_ref_frame_;

    // Last used id
protected:
    int last_used_id_;


    // Init
public:
    int init();

    // Create
public:
    int create();

    //
protected:
    int readParameters();


    //
protected:
    std::string config_file_path_;
    int readConfigFile();

    //
protected:
    double rate_val_;
    ros::Rate* rate_;


    // Open
public:
    int open();

    // Close
public:
    int close();

    // Run
public:
    int run();


};
